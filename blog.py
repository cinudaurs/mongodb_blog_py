import pymongo
import cgi
import re
import datetime
import random
import hmac
import user
import sys
from flask import Flask, request, redirect, make_response, jsonify, abort, render_template, url_for


connection_string = "mongodb://localhost"
app = Flask(__name__)

# inserts the blog entry and returns a permalink for the entry
def insert_entry(title, post, tags_array, author):
    print "inserting blog entry", title, post

    connection = pymongo.Connection(connection_string, safe=True)

    db = connection.blog
    posts = db.posts

    exp = re.compile('\W')  # match anything not alphanumeric
    whitespace = re.compile('\s')
    temp_title = whitespace.sub("_", title)
    permalink = exp.sub('', temp_title)

    post = {"title": title,
            "author": author,
            "body": post,
            "permalink": permalink,
            "tags": tags_array,
            "date": datetime.datetime.utcnow()}

    try:

        posts.insert(post)
        print "Inserting the post"

    except:
        print "Error inserting post"
        print "Unexpected error:", sys.exc_info()[0]

    return permalink


@app.route('/')
def blog_index():
    connection = pymongo.Connection(connection_string, safe=True)
    db = connection.blog
    posts = db.posts

    username = login_check()  # see if user is logged in

    cursor = posts.find().sort('date', direction=-1).limit(10)
    l = []

    for post in cursor:
        post['date'] = post['date'].strftime("%A, %B %d %Y at %I:%M%p")  # fix up date
        if 'tags' not in post:
            post['tags'] = []  # fill it in if its not there already
        if 'comments' not in post:
            post['comments'] = []

        l.append({'title': post['title'], 'body': post['body'], 'post_date': post['date'],
                  'permalink': post['permalink'],
                  'tags': post['tags'],
                  'author': post['author'],
                  'comments': post['comments']})

    return render_template('blog_template.html', myposts=l, username=username)


@app.route('/tag/<tag>', methods=['GET'])
def posts_by_tag(tag="notfound"):
    connection = pymongo.Connection(connection_string, safe=True)
    db = connection.blog
    posts = db.posts

    username = login_check()  # see if user is logged in

    tag = cgi.escape(tag)
    cursor = posts.find({'tags': tag}).sort('date', direction=-1).limit(10)
    l = []

    for post in cursor:
        post['date'] = post['date'].strftime("%A, %B %d %Y at %I:%M%p")  # fix up date
        if 'tags' not in post:
            post['tags'] = []  # fill it in if its not there already
        if 'comments' not in post:
            post['comments'] = []

        l.append({'title': post['title'], 'body': post['body'], 'post_date': post['date'],
                  'permalink': post['permalink'],
                  'tags': post['tags'],
                  'author': post['author'],
                  'comments': post['comments']})

    return render_template('blog_template.html', myposts=l, username=username)


# gets called both for regular requests and json requests
@app.route('/post/<permalink>', methods=['GET'])
def show_post(permalink="notfound"):
    connection = pymongo.Connection(connection_string, safe=True)
    db = connection.blog
    posts = db.posts

    username = login_check()  # see if user is logged in
    permalink = cgi.escape(permalink)

    # determine if its a json request
    path_re = re.compile(r"^([^\.]+).json$")

    print "about to query on permalink = ", permalink
    post = posts.find_one({'permalink': permalink})

    if post is None:
        return redirect('/post_not_found')

    print "date of entry is ", post['date']

    # fix up date
    post['date'] = post['date'].strftime("%A, %B %d %Y at %I:%M%p")

    # init comment form fields for additional comment
    comment = {}
    comment['name'] = ""
    comment['email'] = ""
    comment['body'] = ""

    return render_template("entry_template.html", post=post, username=username, errors="", comment=comment)


# used to process a comment on a blog post
@app.route('/newcomment', methods=['POST'])
def post_newcomment():
    name = request.form.get("commentName")
    email = request.form.get("commentEmail")
    body = request.form.get("commentBody")
    permalink = request.form.get("permalink")


    # look up the post in question
    connection = pymongo.Connection(connection_string, safe=True)
    db = connection.blog
    posts = db.posts

    username = login_check()  # see if user is logged in
    permalink = cgi.escape(permalink)

    post = posts.find_one({'permalink': permalink})
    # if post not found, redirct to post not found error
    if post is None:
        return redirect('/post_not_found')

    # if values not good, redirect to view with errors
    errors = ""
    if name == "" or body == "":

        # fix up date
        post['date'] = post['date'].strftime("%A, %B %d %Y at %I:%M%p")

        # init comment
        comment = {}
        comment['name'] = name
        comment['email'] = email
        comment['body'] = body

        errors = "Post must contain your name and an actual comment."
        print "newcomment: comment contained error..returning form with errors"

        return render_template("entry_template.html", post=post, username=username, errors="", comment=comment)

    else:

        # it all looks good, insert the comment into the blog post and redirect back to the post viewer
        comment = {'author': name}
        if email != "":
            comment['email'] = email
        comment['body'] = body

        try:
            last_error = posts.update({'permalink': permalink}, {'$push': {'comments': comment}}, upsert=False,
                                      manipulate=False, safe=True)

            print "about to update a blog post with a comment"

            # print "num documents updated" + last_error['n']
        except:

            print "Could not update the collection, error"
            print "Unexpected error:", sys.exc_info()[0]

        print "newcomment: added the comment....redirecting to post"

        return redirect('/post/' + permalink)


@app.route('/delete/<permalink>', methods=['GET'])
def del_post(permalink='notfound'):
    # look up the post in question
    connection = pymongo.Connection(connection_string, safe=True)
    db = connection.blog
    posts = db.posts

    username = login_check()  # see if user is logged in
    if username is None:
        return redirect('/login')

    post = posts.find_one({'permalink': permalink})

    if post is None:
        return render_template('delete.html')

    if post is not None:

        try:
            cursor = posts.remove(post)
            print "about to remove the post with permalink  " + permalink

        except:
            print "Could not remove the document from the collection, error"
            print "Unexpected error:", sys.exc_info()[0]

        return redirect('/')


@app.route('/post_not_found', methods=['GET'])
def post_not_found():
    return "Sorry, post not found"


# how new posts are made. this shows the initial page with the form
@app.route('/newpost', methods=['GET'])
def get_newpost():
    username = login_check()  # see if user is logged in
    if username is None:
        return redirect('/login')

    return render_template('newpost.html', subject="", body="", errors="", tags="", username=username)


# extracts the tag from the tags form element. an experience python programmer could do this in  fewer lines, no doubt
def extract_tags(tags):
    whitespace = re.compile('\s')

    nowhite = whitespace.sub("", tags)
    tags_array = nowhite.split(',')

    # let's clean it up
    cleaned = []
    for tag in tags_array:
        if (tag not in cleaned and tag != ""):
            cleaned.append(tag)

    return cleaned


# put handler for setting up a new post
@app.route('/newpost', methods=['POST'])
def post_newpost():
    title = request.form.get("subject")
    post = request.form.get("body")
    tags = request.form.get("tags")

    username = login_check()  # see if user is logged in
    if username is None:
        return redirect('/login')

    if title == "" or post == "":
        errors = "Post must contain a title and blog entry"
        return render_template("newpost.html", subject=cgi.escape(title, quote=True), username=username,
                               body=cgi.escape(post, quote=True), tags=tags, errors=errors)

    # extract tags
    tags = cgi.escape(tags)
    tags_array = extract_tags(tags)

    # looks like a good entry, insert it escaped
    escaped_post = cgi.escape(post, quote=True)

    # substitute some <p> for the paragraph breaks
    newline = re.compile('\r?\n')
    formatted_post = newline.sub("<p>", escaped_post)

    permalink = insert_entry(title, formatted_post, tags_array, username)

    # now redirect to the blog permalink
    return redirect('/post/' + permalink)


# displays the initial blog signup form
@app.route('/signup', methods=['GET'])
def present_signup():
    if request.method == 'GET':
        return render_template("signup.html",
                               username="",
                               password="",
                               password_error="",
                               email="",
                               username_error="",
                               email_error="",
                               verify_error="")


@app.route('/signup', methods=['POST'])
def process_signup():
    connection = pymongo.Connection(connection_string, safe=True)
    email = request.form.get('email')
    print email
    username = request.form.get('username')
    print username
    password = request.form.get('password')
    verify = request.form.get('verify')

    # set these up in case we have an error case
    errors = {'username': cgi.escape(username), 'email': cgi.escape(email)}
    if user.validate_signup(username, password, verify, email, errors):
        if not user.newuser(connection, username, password, email):
            # this was a duplicate
            errors['username_error'] = "Username already in use. Please choose another"
            return render_template('signup.html', errors=errors)

        session_id = user.start_session(connection, username)
        print session_id
        cookie = user.make_secure_val(session_id)
        response = make_response()
        response.set_cookie("session", cookie)
        return redirect('/welcome')
    else:
        print "user did not validate"
        return render_template('signup.html', errors)


# displays the initial blog login form
@app.route('/login', methods=['GET'])
def present_login():
    return render_template("login.html",
                           username="", password="",
                           login_error="")


# handles a login request
@app.route('/login', methods=['POST'])
def process_login():
    connection = pymongo.Connection(connection_string, safe=True)

    username = request.form.get("username")
    password = request.form.get("password")

    print "user submitted ", username, "pass ", password

    user_record = {}

    if user.validate_login(connection, username, password, user_record):
        session_id = user.start_session(connection, username)
        if session_id == -1:
            return redirect(url_for('/internal_error'))

        cookie = user.make_secure_val(session_id)

        response = make_response(render_template('welcome.html'))
        response.set_cookie("session", value=cookie)

        return response

    else:

        return render_template("login.html",
                               username=cgi.escape(username), password="",
                               login_error="Invalid Login")


@app.route('/internal_error', methods=['GET'])
def present_internal_error():
    return app.logger.error('System has encountered a DB error')


@app.route('/logout', methods=['GET'])
def process_logout():
    connection = pymongo.Connection(connection_string, safe=True)

    cookie = request.cookies.get("session")

    if cookie is None:
        print "no cookie..."
        return redirect('/signup')

    else:
        session_id = user.check_secure_val(cookie)

        if session_id is None:
            print "no secure session_id"
            return redirect('/signup')

        else:
            # remove the session

            user.end_session(connection, session_id)

            print "clearing the cookie"

            response = make_response(render_template('signup.html'))
            response.set_cookie("session", "")

            return response


# will check if the user is logged in and if so, return the username. otherwise, it returns None
def login_check():
    connection = pymongo.Connection(connection_string, safe=True)
    cookie = request.cookies.get('session')

    if cookie is None:
        print "no cookie..."
        return None

    else:
        session_id = user.check_secure_val(cookie)

        if session_id is None:
            print "no secure session_id"
            return None

        else:
            # look up username record
            session = user.get_session(connection, session_id)
            if session is None:
                return None
    print session['username']
    return session['username']


@app.route('/welcome', methods=['GET'])
def present_welcome():
    # check for a cookie, if present, then extract value

    username = login_check()
    print "passed login_check"
    if username is None:
        print "welcome: can't identify user...redirecting to signup"
        return redirect('/signup')

    return render_template('welcome.html', username=cgi.escape(username))


if __name__ == '__main__':
    app.run(debug=True)


